package models

import "gorm.io/gorm"

type Alert struct {
	gorm.Model
	SensorID      int
	MeasuredValue float64 `json:"temperature"` // Used for temperature, humidity, and illumination
	Sensor        Sensor
}
