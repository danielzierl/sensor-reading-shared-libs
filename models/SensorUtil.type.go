package models

type Sensor struct {
	ID             int     `json:"id"`
	SensorUUID     string  `json:"sensorUUID"`
	Name           string  `json:"name"`
	Location       string  `json:"location"`
	MinTemperature float64 `json:"minTemperature"` // Used for tempreature, humidity, and illumination -> tree different Sensors (AIMTEC)
	MaxTemperature float64 `json:"maxTemperature"`
}

type MeasurementBody struct {
	CreatedOn   string  `json:"createdOn"`
	SensorUUID  string  `json:"sensorUUID"`
	Temperature float64 `json:"temperature"`
	Status      string  `json:"status"`
}

type AlertBody struct {
	CreatedOn       string  `json:"createdOn"`
	SensorUUID      string  `json:"sensorUUID"`
	Temperature     float64 `json:"temperature"`
	LowTemperature  float64 `json:"lowTemperature"`
	HighTemperature float64 `json:"highTemperature"`
}
