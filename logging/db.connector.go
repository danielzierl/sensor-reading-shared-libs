package logging

import (
	"log"
	"os"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func ConnectDb() *gorm.DB {
	var DB_PATH = os.Getenv("DB_LOG_LOCATION")
	db, err := gorm.Open(sqlite.Open(DB_PATH), &gorm.Config{})
	if err != nil {
		log.Panicf("Failed to connect to logging database at %s, with: %v", DB_PATH, err)
	}
	log.Printf("Database receiver connected")
	return db
}
