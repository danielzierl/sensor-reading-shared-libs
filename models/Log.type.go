package models

import "gorm.io/gorm"

type Log struct {
	gorm.Model
	Emitter  string
	Content  string
	Severity int
}
