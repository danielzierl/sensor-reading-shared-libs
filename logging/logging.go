package logging

import (
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/joho/godotenv"
	"gitlab.com/danielzierl/sensor-reading-shared-libs/models"
	"gorm.io/gorm"
)

type hook struct {
	writer  io.Writer
	emitter string
}

func (h *hook) Write(p []byte) (n int, err error) {
	result := db.Create(&models.Log{Emitter: h.emitter, Content: string(p), Severity: 0})

	if result.Error != nil {
		fmt.Printf("Error creating database logging hook: %s\n", result.Error)
		return 0, result.Error
	}
	return h.writer.Write(p)

}

const LOG_FILE_NAME = "main.log"
const LOG_DIR = "logs"

var logFile *os.File
var db *gorm.DB

func SetupLoggingAuto(emitter string) {
	// Creates new log file in the "git project root"/logs/main.log
	// Needs to be closed in main
	err := godotenv.Load()
	if err != nil {
		log.Panic(".env file not found in the root directory")
	}
	db = ConnectDb()
	rootDir, _ := GetGitRootDir()
	setupLogging(filepath.Join(rootDir, LOG_DIR), emitter)
}

func setupLogging(outputDir string, emitter string) {
	if _, err := os.Stat(outputDir); os.IsNotExist(err) {
		// Directory does not exist, so create it
		err := os.MkdirAll(outputDir, os.ModePerm)
		if err != nil {
			fmt.Printf("Error creating directory at %s with: %v\n", outputDir, err)
		} else {
			fmt.Printf("Directory created: %s\n", outputDir)
		}
	}

	logPath := filepath.Join(outputDir, LOG_FILE_NAME)
	logFile, err := os.OpenFile(logPath, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)

	var multi_writer io.Writer
	if err != nil {
		multi_writer = io.MultiWriter(os.Stdout)
		fmt.Printf("Error opening log file at %s with: %v\n", logPath, err)
	} else {
		multi_writer = io.MultiWriter(os.Stdout, logFile)
	}

	log.SetOutput(&hook{writer: multi_writer, emitter: emitter})
	// log.SetOutput(mw)
	log.Println("Logging setup complete")
}

func CloseLogFile() {
	log.Println("Closing log file")
	if logFile != nil {
		logFile.Close()
	}
}

func GetGitRootDir() (string, error) {
	// Run the "git rev-parse --show-toplevel" command to get the root directory of the Git repository
	cmd := exec.Command("git", "rev-parse", "--show-toplevel")
	output, err := cmd.Output()
	if err != nil {
		return "", err
	}

	// Convert the command output to a string and remove any trailing newline characters
	rootDir := strings.TrimSpace(string(output))

	return rootDir, nil
}
